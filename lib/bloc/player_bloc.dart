import 'package:rxdart/rxdart.dart';
import 'package:weblegends/models/player.dart';
import 'package:weblegends/api/player_api.dart' as PlayerAPI;

class PlayerBloc{
  List<Player> _players;

  BehaviorSubject<List<Player>> _subjectPlayers;

  Stream<List<Player>> get playersStream => _subjectPlayers.stream;

  PlayerBloc(){
    _subjectPlayers = BehaviorSubject<List<Player>>.seeded([]);
    fetchPlayers();
  }

  Future<void> fetchPlayers() async{
    final response = await PlayerAPI.getTopScore();
    _players = response;
    _subjectPlayers.add(_players);
  }

  void close(){
    _subjectPlayers.close();
  }
}

class PlayerBlocFactory{
  static PlayerBloc _playerBloc;

  static PlayerBloc get playerBloc{
    _playerBloc ??= PlayerBloc();
    return _playerBloc;
  }

}