import 'package:rxdart/rxdart.dart';

class LoadingBloc {
  BehaviorSubject<bool> _subjectIsLoading;

  Stream get isLoadingStream => this._subjectIsLoading.stream;

  LoadingBloc() {
    _subjectIsLoading = BehaviorSubject<bool>.seeded(false);
  }

  void setLoading(bool value) {
    this._subjectIsLoading.add(value);
  }

  void close() {
    _subjectIsLoading.close();
  }
}

class LoadingBlocFactory {
  static LoadingBloc _loadingBloc;
  static LoadingBloc get loadingBloc {
    if (_loadingBloc == null) _loadingBloc = LoadingBloc();
    return _loadingBloc;
  }
}
