import 'package:rxdart/rxdart.dart';
import 'package:weblegends/api/battle_api.dart';
import 'package:weblegends/models/enemy.dart';
import 'package:weblegends/models/hero.dart';
import 'dart:convert' as convert;

import 'package:weblegends/models/item.dart';

class HeroBloc {
  List<Item> _items;

  BehaviorSubject<Hero> _subjectHero;
  BehaviorSubject<Enemy> _subjectEnemy;
  BehaviorSubject<String> _subjectBattleLog;
  BehaviorSubject<int> _subjectStatus;
  BehaviorSubject<List<Item>> _subjectItems;

  Stream<Hero> get heroStream => this._subjectHero.stream;
  Stream<Enemy> get enemyStream => this._subjectEnemy.stream;
  Stream<String> get battleLogStream => this._subjectBattleLog.stream;
  Stream<int> get statusStream => this._subjectStatus.stream;
  Stream<List<Item>> get itemsStream => this._subjectItems.stream;
  List<Item> get items => this._items;

  HeroBloc() {
    this._subjectEnemy = BehaviorSubject<Enemy>();
    this._subjectHero = BehaviorSubject<Hero>();
    this._subjectBattleLog = BehaviorSubject<String>();
    this._subjectStatus = BehaviorSubject<int>.seeded(0);
    this._items = List<Item>();
    this._items.add(null);
    this._subjectItems = BehaviorSubject<List<Item>>.seeded(this._items);
  }

  void changeHero() {
    this._subjectHero.add(HeroFactory.hero);
  }

  void changeEnemy() {
    this._subjectEnemy.add(EnemyFactory.enemy);
  }

  void close() {
    _subjectHero.close();
    _subjectEnemy.close();
    _subjectStatus.close();
    _subjectBattleLog.close();
  }

  void changeStatus(int newValue){
    this._subjectStatus.add(newValue);
  }

  void addItem(Map<String, dynamic> item){
    this._items.add(Item.fromJSON(item));
    this._subjectItems.add(this._items);
  }

  void setItem(List<dynamic> items){
    this._items = List<Item>();
    this._items.add(null);
    for(Map<String, dynamic> item in items){
      this.addItem(item);
    }
  }

  void useItem(int index) async {
    Item item = this._items[index];
    final result = await useItemApiCall(item);
    if(result == null) return;
    this._items.removeAt(index);
    this._subjectItems.add(this._items);
    var json = convert.jsonDecode(result);
    HeroFactory.createHero(json['data']);
    this._subjectHero.add(HeroFactory.hero);
    this._subjectBattleLog.add('$item used');
  }

  Future<void> attack() async {
    this._subjectStatus.add(1);
    final result = await attackApiCall();
    if(result == null){
      this._subjectStatus.add(0);
      return;
    }
    var json = convert.jsonDecode(result);
    HeroFactory.createHero(json['data']['player']);
    EnemyFactory.createEnemy(json['data']['enemy']);

    this._subjectHero.add(HeroFactory.hero);
    this._subjectEnemy.add(EnemyFactory.enemy);
    this._subjectBattleLog.add(json['data']['battleLog']);

    await Future.any([
      Future.delayed(
        const Duration(milliseconds: 500),
      ),
    ]);

    if(EnemyFactory.enemy.hp == 0){
      this._subjectStatus.add(2);
      return;
    }

    final res = await enemyAttackApiCall();
    json = convert.jsonDecode(res);

    HeroFactory.createHero(json['data']['player']);
    EnemyFactory.createEnemy(json['data']['enemy']);

    this._subjectHero.add(HeroFactory.hero);
    this._subjectEnemy.add(EnemyFactory.enemy);
    this._subjectBattleLog.add(json['data']['battleLog']);
    if(HeroFactory.hero.hp <= 0){
      this._subjectStatus.add(3);
      return;
    }
    this._subjectStatus.add(0);
  }

  void castSpell() async {
    if(HeroFactory.hero.mana < HeroFactory.hero.spell.mana){
      this._subjectBattleLog.add('Not enough mana');
      return;
    }
    this._subjectStatus.add(1);
    final result = await spellApiCall();
    var json = convert.jsonDecode(result);
    HeroFactory.createHero(json['data']['player']);
    EnemyFactory.createEnemy(json['data']['enemy']);

    this._subjectHero.add(HeroFactory.hero);
    this._subjectEnemy.add(EnemyFactory.enemy);
    this._subjectBattleLog.add(json['data']['battleLog']);

    await Future.any([
      Future.delayed(
        const Duration(milliseconds: 500),
      ),
    ]);

    if(EnemyFactory.enemy.hp == 0){
      this._subjectStatus.add(2);
      return;
    }

    final res = await enemyAttackApiCall();
    json = convert.jsonDecode(res);

    HeroFactory.createHero(json['data']['player']);
    EnemyFactory.createEnemy(json['data']['enemy']);

    this._subjectHero.add(HeroFactory.hero);
    this._subjectEnemy.add(EnemyFactory.enemy);
    this._subjectBattleLog.add(json['data']['battleLog']);
    if(HeroFactory.hero.hp <= 0){
      this._subjectStatus.add(3);
      return;
    }
    this._subjectStatus.add(0);
  }

  void init(){
    this._subjectStatus.add(0);
    this._subjectBattleLog.add('');
    changeEnemy();
    changeHero();
  }
}



class HeroBlocFactory {
  static HeroBloc _heroBloc;
  static HeroBloc get heroBloc {
    if (_heroBloc == null) _heroBloc = HeroBloc();
    return _heroBloc;
  }
}
