import 'package:rxdart/rxdart.dart';
import 'package:weblegends/api/shared_preferences.dart';
import 'package:weblegends/models/account.dart';
import 'dart:convert' as convert;

class AccountBloc {
  Account _account;
  bool _isAuthenticated;

  BehaviorSubject<Account> _subjectAccount;
  BehaviorSubject<bool> _subjectIsAuthenticated;

  Stream<Account> get accountStream => this._subjectAccount.stream;
  Stream<bool> get isAuthenticatedStream => this._subjectIsAuthenticated.stream;

  AccountBloc() {
    this._account = null;
    this._subjectAccount = BehaviorSubject<Account>.seeded(_account);
    this._subjectIsAuthenticated = BehaviorSubject<bool>.seeded(false);
    auth();
  }

  Future<void> auth() async {
    final result = await SharedPreferencesFactory.login;
    if (result == null) return;
    Account account = Account.fromJSON(convert.jsonDecode(result));
    await login(account.username, account.password);
  }

  Future<void> login(String username, String password) async {
    await AccountFactory.login(username, password);
  }

  Future<void> register(String username, String email, String password) async {
    await AccountFactory.register(username, email, password);
  }

  Future<void> startNewGame(String heroClass) async {
    await AccountFactory.startNewGame(heroClass);
  }

  void close() {
    _subjectAccount.close();
    _subjectIsAuthenticated.close();
  }

  void loginAccount(Account account) {
    this._account = account;
    this._subjectAccount.add(this._account);
    this._isAuthenticated = true;
    this._subjectIsAuthenticated.add(this._isAuthenticated);
    SharedPreferencesFactory.saveLogin(convert.jsonEncode(account.toJSON()));
  }

  void logoutAccount() {
    this._account = null;
    this._isAuthenticated = false;
    this._subjectIsAuthenticated.add(this._isAuthenticated);
    this._subjectAccount.add(this._account);
    SharedPreferencesFactory.deleteLogin();
  }
}

class AccountBlocFactory {
  static AccountBloc _accountBloc;
  static AccountBloc get accountBloc {
    if (_accountBloc == null) {
      _accountBloc = AccountBloc();
    }
    return _accountBloc;
  }
}
