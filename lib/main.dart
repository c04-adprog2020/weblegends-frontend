import 'package:flutter/material.dart';
import 'package:weblegends/bloc/player_bloc.dart';
import 'package:weblegends/models/player.dart';
import 'package:weblegends/ui/battle.dart';
import 'package:weblegends/ui/home.dart';
import 'package:weblegends/ui/result.dart';
import 'package:weblegends/ui/result_lost.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Web Legends',
      theme: ThemeData.dark(),
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/': (_) => MyHomePage(),
        '/battle': (_) => MyBattlePage(),
        '/result': (_) => MyResultPage(),
        '/result-lost': (_) => MyResultLostPage(),
      },
    );
  }
}

class MyNavbarItem extends StatelessWidget {
  const MyNavbarItem({Key key, this.text, this.onPressHandler})
      : super(key: key);

  final String text;
  final Function onPressHandler;

  @override
  Widget build(BuildContext context) {
    return FlatButton(onPressed: onPressHandler, child: Text(text));
  }
}

class MyLeaderBoard extends StatelessWidget {
  const MyLeaderBoard({Key key}) : super(key: key);

  List<Row> _buildTable(List<Player> data) {
    List<Row> ret = List<Row>();
    ret.add(
      Row(
        children: [
          Expanded(
            flex: 1,
            child: Container(
              color: ThemeData.dark().primaryColor,
              padding: EdgeInsets.all(10.0),
              child: Text('#'),
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(
              color: ThemeData.dark().primaryColor,
              padding: EdgeInsets.all(10.0),
              child: Text('Username'),
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(
              color: ThemeData.dark().primaryColor,
              padding: EdgeInsets.all(10.0),
              child: Text('Hero Class'),
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(
              color: ThemeData.dark().primaryColor,
              padding: EdgeInsets.all(10.0),
              child: Text('Score'),
            ),
          ),
        ],
      ),
    );
    if (data == null) return ret;
    int index = 0;
    data.forEach((element) {
      index += 1;
      ret.add(
        Row(
          children: [
            Expanded(
              flex: 1,
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(width: 0.5),
                ),
                padding: EdgeInsets.all(10.0),
                child: Text('$index'),
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(width: 0.5),
                ),
                padding: EdgeInsets.all(10.0),
                child: Text('${element.name}'),
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(width: 0.5),
                ),
                padding: EdgeInsets.all(10.0),
                child: Text('${element.heroClass}'),
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(width: 0.5),
                ),
                padding: EdgeInsets.all(10.0),
                child: Text('${element.score}'),
              ),
            ),
          ],
        ),
      );
    });
    return ret;
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: PlayerBlocFactory.playerBloc.playersStream,
      builder: (context, AsyncSnapshot<List<Player>> snapshot) {
        return Column(
          // border: TableBorder.all(
          // style: BorderStyle.solid,
          // width: 1.0,
          // color: Colors.cyan.shade900,
          // ),
          children: _buildTable(snapshot.data),
        );
        // children:
      },
    );
  }
}
