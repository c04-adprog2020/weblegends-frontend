const String kServerUrl = 'https://serverweblegends.herokuapp.com';
// const String kServerUrl = 'http://localhost:4000';
List<String> kHeroList = <String>[
  'Archer',
  'Assassin',
  'Cleric',
  'Knight',
  'Mage',
  'Ranger'
];
const double kHealthBarWidth = 500;
const int kEnemySkillCooldown = 4;