import 'package:weblegends/models/spell.dart';
import 'package:weblegends/models/weapon.dart';

class Hero {
  String _className, _name, _type;
  int _maxHP, _maxMana, _hp, _mana, _defense, _damage;
  Weapon _weapon;
  Spell _spell;

  Hero(
      {className,
      name,
      type,
      maxHP,
      maxMana,
      hp,
      mana,
      weapon,
      spell,
      defense,
      damage}) {
    this._type = type;
    this._name = name;
    this._maxHP = maxHP;
    this._maxMana = maxMana;
    this._weapon = weapon;
    this._hp = hp;
    this._mana = mana;
    this._spell = spell;
    this._className = className;
    this._defense = defense;
    this._damage = damage;
  }

  Hero.fromJSON(Map<String, dynamic> map) {
    this._type = map['type'];
    this._name = map['name'];
    this._maxHP = map['maxHP'];
    this._maxMana = map['maxMana'];
    this._weapon = Weapon.fromJSON(map['weapon']);
    this._hp = map['hp'];
    this._mana = map['mana'];
    this._spell = Spell.fromJSON(map['spell']);
    this._className = map['className'];
    this._defense = map['defense'];
    this._damage = map['damage'];
  }

  Map<String, dynamic> toJSON() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['type'] = this._type;
    map['name'] = this._name;
    map['maxHP'] = this._maxHP;
    map['maxMana'] = this._maxMana;
    map['weapon'] = this._weapon.toJSON();
    map['hp'] = this._hp;
    map['mana'] = this._mana;
    map['spell'] = this._spell.toJSON();
    map['className'] = this._className;
    map['defense'] = this._defense;
    map['damage'] = this._damage;
    return map;
  }

  String get type => this._type;
  String get name => this._name;
  int get maxHP => this._maxHP;
  int get maxMana => this._maxMana;
  Weapon get weapon => this._weapon;
  int get hp => this._hp;
  int get mana => this._mana;
  Spell get spell => this._spell;
  String get className => this._className;
  int get defense => this._defense;
  int get damage => this._damage;
}

class HeroFactory {
  static Hero _hero;
  static Hero createHero(Map<String, dynamic> map) {
    if (_hero != null && !map.containsKey('type')) map['type'] = _hero.type;
    _hero = Hero.fromJSON(map);
    return _hero;
  }

  static Hero get hero => _hero;
}
