class Weapon {
  String _type, _name, _weaponType;
  int _attack, _defense;
  Weapon(
      {String type, String name, String weaponType, int attack, int defense}) {
    this._type = type;
    this._name = name;
    this._weaponType = weaponType;
    this._attack = attack;
    this._defense = defense;
  }

  Weapon.fromJSON(Map<String, dynamic> map){
    this._type = map['type'];
    this._name = map['name'];
    this._weaponType = map['weaponType'];
    this._attack = map['attack'];
    this._defense = map['defense'];
  }

  Map<String, dynamic> toJSON(){
    Map<String, dynamic> map = Map<String, dynamic>();
    map['type'] = this._type;
    map['attack'] = this._attack;
    map['defense'] = this._defense;
    map['name'] = this._name;
    map['weaponType'] = this._weaponType;
    return map;
  }

  String get type => this._type;
  String get name => this._name;
  String get weaponType => this._weaponType;
  int get attack => this._attack;
  int get defense => this._defense;
}
