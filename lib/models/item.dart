class Item{
  String _type, _itemType;
  double _percent;

  Item.fromJSON(Map<String, dynamic> map){
    this._type = map['type'];
    this._itemType = map['itemType'];
    this._percent = map['percent'];
  }

  Map<String, dynamic> toJSON(){
    Map<String, dynamic> map = {
      'type' : this._type,
      'itemType' : this._itemType,
      'percent' : this._percent,
    };
    return map;
  }

  String toString(){
    return 'Item ${this._type} ${this._percent}';
  }

}