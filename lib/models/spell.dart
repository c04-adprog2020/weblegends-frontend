class Spell{
  String _type, _name;
  int _mana, _cooldown, _cooldownCounter;
  Spell({String type, String name, int mana, int cooldown, int cooldownCounter}){
    this._type = type;
    this._name = name;
    this._mana = mana;
    this._cooldown = cooldown;
    this._cooldownCounter = cooldownCounter;
  }

  Spell.fromJSON(Map<String, dynamic> map){
    this._type = map['type'];
    this._name = map['name'];
    this._mana = map['mana'];
    this._cooldown = map['cooldown'];
    this._cooldownCounter = map['cooldownCounter'];
  }

  Map<String, dynamic> toJSON(){
    Map<String, dynamic> map = Map<String, dynamic>();
    map['type'] = this._type;
    map['name'] = this._name;
    map['mana'] = this.mana;
    map['cooldown'] = this._cooldown;
    map['cooldownCounter'] = this._cooldownCounter;
    return map;
  }

  String get type => this._type;
  String get name => this._name;
  int get mana => this._mana;
  int get cooldown => this._cooldown;
  int get cooldownCounter => this._cooldownCounter;

}