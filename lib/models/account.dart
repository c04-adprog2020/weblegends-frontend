import 'dart:convert' as convert;

import 'package:weblegends/api/account_api.dart';
import 'package:weblegends/api/battle_api.dart';
import 'package:weblegends/bloc/account_bloc.dart';
import 'package:weblegends/bloc/hero_bloc.dart';
import 'package:weblegends/bloc/player_bloc.dart';
import 'package:weblegends/models/enemy.dart';
import 'package:weblegends/models/hero.dart';

class Account {
  String _username;
  String _email;
  String _password;
  String _gameId;
  int _score;

  Account(
      this._username, this._email, this._password, this._gameId, this._score);

  Account.fromJSON(Map<String, dynamic> map) {
    this._username = map['username'];
    this._email = map['email'];
    this._password = map['password'];
    this._gameId = map['gameId'];
    this._score = map['score'];
  }

  Map<String, dynamic> toJSON() {
    Map<String, dynamic> map = {
      'username': this._username,
      'password': this._password,
      'email': this._email,
      'gameId': this._gameId,
      'score': this._score,
    };
    return map;
  }

  String get username => this._username;
  String get password => this._password;
  String get email => this._email;
  String get gameId => this._gameId;
  int get score => this._score;

  set gameId(String newValue) => this._gameId = newValue;
  set score(int newValue) => this._score = newValue;
  void addScore() => this._score += 1;
}

class AccountFactory {
  static Account _account;

  static Account get account => _account;

  AccountFactory() {
    _account = null;
  }

  static Future<void> login(String username, String password) async {
    final response = await loginApiCall(username, password);
    if (response == null) return;
    var json = convert.jsonDecode(response)['account'];
    _account = Account(json['username'], json['email'], json['password'],
        json['gameId'], json['score']);
    AccountBlocFactory.accountBloc.loginAccount(_account);
  }

  static Future<void> register(
      String username, String email, String password) async {
    final response = await registerApiCall(username, email, password);
    if (response == null) return;
    var json = convert.jsonDecode(response)['account'];
    _account = Account(json['username'], json['email'], json['password'],
        json['gameId'], json['score']);
    AccountBlocFactory.accountBloc.loginAccount(_account);
  }

  static Future<void> startNewGame(String heroClass) async {
    final response = await startNewGameApiCall(heroClass);
    final result = await spawnEnemyApiCall(1);
    if (response == null || result == null) return;
    var json = convert.jsonDecode(response);
    var map = convert.jsonDecode(result);
    AccountFactory.account.gameId = json['id'];
    AccountFactory.account.score = 0;
    HeroFactory.createHero(json['data']);
    EnemyFactory.createEnemy(map['data']);

    HeroBlocFactory.heroBloc.init();
    PlayerBlocFactory.playerBloc.fetchPlayers();
  }

  static Future<void> loadSavedGame() async {
    final response = await loadHeroApiCall();
    final result = await spawnEnemyApiCall(AccountFactory.account.score + 1);
    if (response == null || result == null) return;
    var json = convert.jsonDecode(response);
    var map = convert.jsonDecode(result);
    HeroFactory.createHero(json['data']['hero']);
    HeroBlocFactory.heroBloc.setItem(json['data']['items']);
    EnemyFactory.createEnemy(map['data']);
    HeroBlocFactory.heroBloc.init();
  }

  static Future<void> completeLevel() async {
    AccountFactory.account.addScore();
    final response = await updateScoreApiCall(AccountFactory.account.score);
    if (!response) return;
    final result = await upgradeHeroApiCall();
    if (result == null) return;
    final res = await dropItemApiCall();
    if(res == null) return;
    var json = convert.jsonDecode(res);
    HeroBlocFactory.heroBloc.addItem(json['data']);
    var map = convert.jsonDecode(result);
    HeroFactory.createHero(map['data']);
    await saveHeroApiCall();
  }

  static Future<void> nextGame() async {
    await completeLevel();
    final result = await spawnEnemyApiCall(EnemyFactory.enemy.level + 1);
    if (result == null) return;
    var map = convert.jsonDecode(result);
    EnemyFactory.createEnemy(map['data']);
    HeroBlocFactory.heroBloc.init();
    PlayerBlocFactory.playerBloc.fetchPlayers();
  }
}
