class Player{

  String _name, _key, _heroClass;
  int _score;

  Player(String name, String key, String heroClass, int score){
    this._name = name;
    this._key = key;
    this._heroClass = heroClass;
    this._score = score;
  }

  String get name => this._name;
  String get key => this._key;
  String get heroClass => this._heroClass;
  int get score => this._score;

  set name(String newValue) => this._name = newValue;
  set key(String newValue) => this._key = newValue;
  set heroClass(String newValue) => this._heroClass = newValue;
  set score(int newValue) => this._score = newValue;

}

class PlayerFactory{
  static Player createPlayer(Map<String, dynamic> json){
    Player player = Player(json['name'], json['key'], json['heroClass'], json['score']);
    return player;
  }
}