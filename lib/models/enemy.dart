class Enemy {
  String _type;
  double _evasionRate;
  int _level, _maxHP, _skillCooldown, _accuracy;
  String _currentMove, _moveDescription;
  int _defense, _damage, _hp;

  Enemy();

  Enemy.fromJSON(Map<String, dynamic> map) {
    this._type = map['type'];
    this._level = map['level'];
    this._maxHP = map['maxHP'];
    this._skillCooldown = map['skillCooldown'];
    this._evasionRate = map['evasionRate'];
    this._accuracy = map['accuracy'];
    this._currentMove = map['currentMove'];
    this._moveDescription = map['moveDescription'];
    this._defense = map['defense'];
    this._damage = map['damage'];
    this._hp = map['hp'];
  }

  Map<String, dynamic> toJSON() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['type'] = this._type;
    map['level'] = this._level;
    map['maxHP'] = this._maxHP;
    map['skillCooldown'] = this._skillCooldown;
    map['type'] = this._type;
    map['evasionRate'] = this._evasionRate;
    map['accuracy'] = this._accuracy;
    map['currentMove'] = this._currentMove;
    map['moveDescription'] = this._moveDescription;
    map['defense'] = this._defense;
    map['damage'] = this._damage;
    map['hp'] = this._hp;
    return map;
  }

  String get type => this._type;
  int get level => this._level;
  int get maxHP => this._maxHP;
  int get hp => this._hp;
  int get skillCooldown => this._skillCooldown;
  int get damage => this._damage;
  int get defense => this._defense;
}

class EnemyFactory{
  static Enemy _enemy;
  static Enemy get enemy => _enemy;
  static Enemy createEnemy(Map<String, dynamic> map) {
    if(_enemy != null && !map.containsKey('type'))
      map['type'] = _enemy.type;
    _enemy = Enemy.fromJSON(map);
    return _enemy;
  }
}