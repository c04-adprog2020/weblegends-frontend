import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesFactory {
  static Future<String> get login async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String loginInformation = prefs.getString('loginInformation');
    return loginInformation;
  }

  static Future<void> saveLogin(String loginInformation) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('loginInformation', loginInformation);
  }

  static Future<void> deleteLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove('loginInformation');
  }
}
