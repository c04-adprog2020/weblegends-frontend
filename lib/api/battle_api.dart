import 'dart:convert' as convert;

import 'package:weblegends/bloc/hero_bloc.dart';
import 'package:weblegends/const.dart';
import 'package:weblegends/models/account.dart';
import 'package:weblegends/models/enemy.dart';
import 'package:weblegends/models/hero.dart';
import 'package:http/http.dart' as http;
import 'package:weblegends/models/item.dart';

const String _urlAttack = '$kServerUrl/api/attack';

Future<String> attackApiCall() async {
  try {
    final Map<String, dynamic> body = {
      'player': HeroFactory.hero.toJSON(),
      'enemy': EnemyFactory.enemy.toJSON(),
    };
    final response = await http.post(
      _urlAttack,
      body: convert.jsonEncode(body),
      headers: {'Content-type': 'application/json'},
    );
    if (response.statusCode != 200) throw ('Error');
    return response.body;
  } catch (err) {
    return null;
  }
}

const String _urlSpell = '$kServerUrl/api/skill';

Future<String> spellApiCall() async {
  try {
    final Map<String, dynamic> body = {
      'player': HeroFactory.hero.toJSON(),
      'enemy': EnemyFactory.enemy.toJSON(),
    };
    final response = await http.post(
      _urlSpell,
      body: convert.jsonEncode(body),
      headers: {'Content-type': 'application/json'},
    );
    if (response.statusCode != 200) throw ('Error');
    return response.body;
  } catch (err) {
    return null;
  }
}

const String _urlEnemyAttack = '$kServerUrl/api/enemy-attack';

Future<String> enemyAttackApiCall() async {
  try {
    final Map<String, dynamic> body = {
      'player': HeroFactory.hero.toJSON(),
      'enemy': EnemyFactory.enemy.toJSON(),
    };
    final response = await http.post(
      _urlEnemyAttack,
      body: convert.jsonEncode(body),
      headers: {'Content-type': 'application/json'},
    );
    if (response.statusCode != 200) throw ('Error');
    return response.body;
  } catch (err) {
    return null;
  }
}

const String _urlUpgradeHero = '$kServerUrl/api/upgrade';

Future<String> upgradeHeroApiCall() async {
  try {
    final Map<String, dynamic> body = HeroFactory.hero.toJSON();
    final response = await http.post(
      _urlUpgradeHero,
      body: convert.jsonEncode(body),
      headers: {'Content-type': 'application/json'},
    );
    if (response.statusCode != 200) throw ('Error');
    return response.body;
  } catch (err) {
    return null;
  }
}

const String _urlSaveHero = '$kServerUrl/api/save-hero';

Future<bool> saveHeroApiCall() async {
  try {
    List<Map<String, dynamic>> itemsJSON = List<Map<String, dynamic>>();
    HeroBlocFactory.heroBloc.items.forEach(
        (element) => {if (element != null) itemsJSON.add(element.toJSON())});

    final Map<String, dynamic> body = {
      'gameId': AccountFactory.account.gameId,
      'hero': HeroFactory.hero.toJSON(),
      'items': itemsJSON
    };
    final response = await http.post(
      _urlSaveHero,
      body: convert.jsonEncode(body),
      headers: {'Content-type': 'application/json'},
    );
    if (response.statusCode != 200) throw ('Error');
    return true;
  } catch (err) {
    return false;
  }
}

const String _urlLoadHero = '$kServerUrl/api/load-hero';

Future<String> loadHeroApiCall() async {
  try {
    final Map<String, dynamic> body = {'gameId': AccountFactory.account.gameId};
    final response = await http.post(
      _urlLoadHero,
      body: convert.jsonEncode(body),
      headers: {'Content-type': 'application/json'},
    );
    if (response.statusCode != 200) throw ('Error');
    return response.body;
  } catch (err) {
    return null;
  }
}

const String _urlUseItem = '$kServerUrl/api/use-item';

Future<String> useItemApiCall(Item item) async {
  try {
    final Map<String, dynamic> body = {
      'item': item.toJSON(),
      'character': HeroFactory.hero.toJSON()
    };
    final response = await http.post(
      _urlUseItem,
      body: convert.jsonEncode(body),
      headers: {'Content-type': 'application/json'},
    );
    if (response.statusCode != 200) throw ('Error');
    return response.body;
  } catch (err) {
    return null;
  }
}
