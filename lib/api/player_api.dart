import 'dart:convert' as convert;

import 'package:weblegends/const.dart';
import 'package:weblegends/models/player.dart';
import 'package:http/http.dart' as http;

const String _url = '$kServerUrl/api/leaderboard';

Future<List<Player>> getTopScore() async {
  try {
    final response = await http.get(_url);
    List<dynamic> json = convert.jsonDecode(response.body);
    List<Player> ret = List<Player>();
    for(dynamic x in json){
      ret.add(PlayerFactory.createPlayer(x));
    }
    return ret;
  } catch (err) {
    return [];
  }
}
