import 'package:weblegends/const.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

import 'package:weblegends/models/account.dart';

const String _urlLogin = '$kServerUrl/api/login/';

Future<String> loginApiCall(String username, String password) async {
  try {
    final Map<String, String> body = {
      'username': username,
      'password': password,
    };
    final response = await http.post(
      _urlLogin,
      body: convert.jsonEncode(body),
      headers: {'Content-type': 'application/json'},
    );
    if (response.statusCode != 200) throw (response.body);
    return response.body;
  } catch (err) {
    return null;
  }
}

const String _urlRegister = '$kServerUrl/api/register/';

Future<String> registerApiCall(
    String username, String email, String password) async {
  try {
    final Map<String, String> body = {
      'username': username,
      'email': email,
      'password': password,
    };
    final response = await http.post(
      _urlRegister,
      body: convert.jsonEncode(body),
      headers: {'Content-type': 'application/json'},
    );
    if (response.statusCode != 200) throw (response.body);
    return response.body;
  } catch (err) {
    return null;
  }
}

const String _urlStartNewGame = '$kServerUrl/api/create/';

Future<String> startNewGameApiCall(String heroClass) async {
  try {
    final Map<String, String> body = {
      'username': AccountFactory.account.username,
      'heroClass': heroClass,
    };
    final response = await http.post(
      _urlStartNewGame,
      body: convert.jsonEncode(body),
      headers: {'Content-type': 'application/json'},
    );
    if (response.statusCode != 200) throw (response.body);
    return response.body;
  } catch (err) {
    return null;
  }
}

const String _urlSpawnEnemy = '$kServerUrl/api/spawn-enemy/';

Future<String> spawnEnemyApiCall(int level) async {
  try {
    final Map<String, dynamic> body = {
      'level': level,
    };
    final response = await http.post(
      _urlSpawnEnemy,
      body: convert.jsonEncode(body),
      headers: {'Content-type': 'application/json'},
    );
    if (response.statusCode != 200) throw ('Error');
    return response.body;
  } catch (err) {
    return null;
  }
}

const String _urlDropItem = '$kServerUrl/api/drop-item/';

Future<String> dropItemApiCall() async {
  try {
    final Map<String, dynamic> body = {};
    final response = await http.post(
      _urlDropItem,
      body: convert.jsonEncode(body),
      headers: {'Content-type': 'application/json'},
    );
    if (response.statusCode != 200) throw ('Error');
    return response.body;
  } catch (err) {
    return null;
  }
}

const String _urlUpdateScore = '$kServerUrl/api/update-score/';

Future<bool> updateScoreApiCall(int newScore) async {
  try {
    final Map<String, dynamic> body = {
      'username': AccountFactory.account.username,
      'newScore': newScore,
    };
    final response = await http.post(
      _urlUpdateScore,
      body: convert.jsonEncode(body),
      headers: {'Content-type': 'application/json'},
    );
    if (response.statusCode != 200) throw ('Error');
    return true;
  } catch (err) {
    return false;
  }
}
