import 'package:flutter/material.dart';
import 'package:weblegends/bloc/account_bloc.dart';
import 'package:weblegends/main.dart';
import 'package:weblegends/models/account.dart';

class MyResultLostPage extends StatefulWidget {
  MyResultLostPage({Key key}) : super(key: key);

  @override
  _MyResultLostPageState createState() => _MyResultLostPageState();
}

class _MyResultLostPageState extends State<MyResultLostPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.games),
          onPressed: () {
            Navigator.pushReplacementNamed(context, '/');
          },
        ),
        actions: [
          MyNavbarItem(
            text: 'Logout',
            onPressHandler: () {
              AccountBlocFactory.accountBloc.logoutAccount();
              Navigator.pushReplacementNamed(context, '/');
            },
          ),
        ],
      ),
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'You Lost!',
                style: TextStyle(fontSize: 35, color: Colors.green),
              ),
              SizedBox(height: 30),
              RaisedButton(
                onPressed: () async {
                  await AccountFactory.loadSavedGame();
                  Navigator.pushNamed(context, '/');
                },
                child: Text('Exit game'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
