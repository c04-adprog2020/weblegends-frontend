import 'package:flutter/material.dart';
import 'package:weblegends/bloc/account_bloc.dart';

Future<void> showRegisterDialog(BuildContext context) {
  TextEditingController _username = TextEditingController();
  TextEditingController _password = TextEditingController();
  TextEditingController _email = TextEditingController();
  return showDialog<void>(
    context: context,
    builder: (_) => AlertDialog(
      title: Text('Register'),
      actions: [
        FlatButton(
          onPressed: () {
            AccountBlocFactory.accountBloc
                .register(_username.text, _email.text, _password.text);
            Navigator.pop(context);
          },
          child: Text("Register"),
        ),
      ],
      actionsPadding: EdgeInsets.all(0),
      content: Container(
        height: 150,
        child: Column(
          children: [
            // Expanded(child: Text("Username")),
            Expanded(
              child: TextFormField(
                controller: _username,
                maxLines: 1,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(0),
                  icon: Icon(Icons.account_circle),
                  labelText: 'Username',
                ),
              ),
            ),
            Expanded(
              child: TextFormField(
                controller: _email,
                maxLines: 1,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(0),
                  icon: Icon(Icons.email),
                  labelText: 'Email',
                ),
              ),
            ),
            Expanded(
              child: TextFormField(
                controller: _password,
                maxLines: 1,
                obscureText: true,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(0),
                  icon: Icon(Icons.security),
                  labelText: 'Password',
                ),
              ),
            ),
          ],
        ),
      ),
    ),
    barrierDismissible: true,
  );
}
