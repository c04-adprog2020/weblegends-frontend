import 'package:flutter/material.dart';
import 'package:weblegends/bloc/account_bloc.dart';
import 'package:weblegends/main.dart';
import 'package:weblegends/models/account.dart';
import 'package:weblegends/ui/login.dart';
import 'package:weblegends/ui/register.dart';
import 'package:weblegends/ui/select_hero.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(icon: Icon(Icons.games), onPressed: null),
        actions: [
          StreamBuilder(
            initialData: false,
            stream: AccountBlocFactory.accountBloc.isAuthenticatedStream,
            builder: (context, snapshot) {
              if (!snapshot.data)
                return MyNavbarItem(
                  text: 'Login',
                  onPressHandler: () {
                    showLoginDialog(context);
                  },
                );
              return MyNavbarItem(
                text: 'Logout',
                onPressHandler: () {
                  AccountBlocFactory.accountBloc.logoutAccount();
                },
              );
            },
          ),
          StreamBuilder(
            initialData: false,
            stream: AccountBlocFactory.accountBloc.isAuthenticatedStream,
            builder: (context, snapshot) {
              if (!snapshot.data)
                return MyNavbarItem(
                  text: 'Register',
                  onPressHandler: () {
                    showRegisterDialog(context);
                  },
                );
              return Container();
            },
          ),
        ],
      ),
      body: Container(
        child: Row(
          children: [
            Expanded(
              flex: 3,
              child: Container(
                padding: const EdgeInsets.only(top: 130.0),
                child: StreamBuilder(
                  stream: AccountBlocFactory.accountBloc.isAuthenticatedStream,
                  builder: (context, snapshot) {
                    if (!snapshot.hasData || !snapshot.data) {
                      return Column(
                        children: [
                          MainMenuButton(
                            text: 'Login',
                            onPress: () {
                              showLoginDialog(context);
                            },
                          ),
                          MainMenuButton(
                            text: 'Register',
                            onPress: () {
                              showRegisterDialog(context);
                            },
                          ),
                          MainMenuButton(
                            text: 'Tutorial',
                            onPress: () {},
                          ),
                        ],
                      );
                    }
                    return Column(
                      children: [
                        MainMenuButton(
                          text: 'Start New Game',
                          onPress: () async {
                            await showHeroesSelector(context);
                          },
                        ),
                        _isLoading
                            ? CircularProgressIndicator()
                            : MainMenuButton(
                                text: 'Load Game',
                                onPress: AccountFactory.account.score == 0
                                    ? null
                                    : () async {
                                        setState(() {
                                          _isLoading = true;
                                        });
                                        await AccountFactory.loadSavedGame();
                                        Navigator.pushNamed(context, '/battle');
                                      },
                              ),
                        MainMenuButton(
                          //TODO Tutorial if necessary
                          text: 'Tutorial',
                          onPress: () {},
                        ),
                        MainMenuButton(
                          text: 'Logout',
                          onPress: () {
                            AccountBlocFactory.accountBloc.logoutAccount();
                          },
                        ),
                      ],
                    );
                  },
                ),
              ),
            ),
            Container(
              width: 430,
              padding: const EdgeInsets.only(top: 110.0, right: 40),
              child: MyLeaderBoard(),
            ),
          ],
        ),
      ),
    );
  }
}

class MainMenuButton extends StatelessWidget {
  const MainMenuButton({Key key, this.text, this.onPress}) : super(key: key);

  final String text;
  final Function onPress;

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: onPress,
      color: ThemeData.dark().accentColor,
      child: Container(
        width: 200,
        child: Center(
          child: Text(
            text,
            style: TextStyle(color: Colors.black),
          ),
        ),
      ),
    );
  }
}
