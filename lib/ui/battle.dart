import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:weblegends/bloc/account_bloc.dart';
import 'package:weblegends/bloc/hero_bloc.dart';
import 'package:weblegends/const.dart';
import 'package:weblegends/main.dart';
import 'package:weblegends/models/account.dart';
import 'package:weblegends/models/enemy.dart';
import 'package:weblegends/models/hero.dart' as Hero;
import 'package:percent_indicator/percent_indicator.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:weblegends/models/item.dart';

class MyBattlePage extends StatefulWidget {
  MyBattlePage({Key key}) : super(key: key);

  @override
  _MyBattlePageState createState() => _MyBattlePageState();
}

class _MyBattlePageState extends State<MyBattlePage> {
  int dropDownValue = 0;

  List<DropdownMenuItem<int>> _listDropwdownMenu(List<Item> items) {
    List<DropdownMenuItem<int>> ret = List<DropdownMenuItem<int>>();
    items.asMap().forEach((key, value) {
      ret.add(
        DropdownMenuItem<int>(
          child: Text(key == 0 ? 'none' : value.toString()),
          value: key,
        ),
      );
    });
    return ret;
  }

  @override
  Widget build(BuildContext context) {
    HeroBlocFactory.heroBloc.statusStream.listen((event) async {
      if (event == 2) {
        Navigator.pushReplacementNamed(context, '/result');
      }
      if (event == 3) {
        Navigator.pushReplacementNamed(context, '/result-lost');
      }
    });

    return StreamBuilder(
      stream: HeroBlocFactory.heroBloc.statusStream,
      builder: (context, AsyncSnapshot<int> snapshot) {
        int statusData = snapshot.hasData ? snapshot.data : 0;
        return Scaffold(
          appBar: AppBar(
            leading: StreamBuilder(
              stream: HeroBlocFactory.heroBloc.enemyStream,
              builder: (context, AsyncSnapshot<Enemy> snapshot) {
                if (!snapshot.hasData) {
                  return IconButton(
                    icon: Icon(Icons.games),
                    onPressed: () {
                      Navigator.pushReplacementNamed(context, '/');
                    },
                  );
                }
                return Container(
                  margin: EdgeInsets.only(left: 10.0),
                  child: Center(
                    child: GestureDetector(
                      child: Text('Level ${snapshot.data.level}'),
                      onTap: () {
                        Navigator.pushReplacementNamed(context, '/');
                      },
                    ),
                  ),
                );
              },
            ),
            actions: [
              MyNavbarItem(
                text: 'Logout',
                onPressHandler: () {
                  AccountBlocFactory.accountBloc.logoutAccount();
                  Navigator.pushReplacementNamed(context, '/');
                },
              ),
            ],
          ),
          body: Container(
            child: Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: StreamBuilder(
                          stream: HeroBlocFactory.heroBloc.enemyStream,
                          builder: (context, AsyncSnapshot<Enemy> snapshot) {
                            if (!snapshot.hasData) return Container();
                            return Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'Enemy Status',
                                  style: TextStyle(
                                    fontSize: 25.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                SizedBox(height: 15.0),
                                ListTile(
                                  leading: Icon(Icons.account_circle),
                                  title: Text('Type'),
                                  trailing: Text(snapshot.data.type),
                                ),
                                SizedBox(height: 5.0),
                                ListTile(
                                  leading: Icon(MdiIcons.swordCross),
                                  title: Text('Attack'),
                                  trailing:
                                      Text(snapshot.data.damage.toString()),
                                ),
                                SizedBox(height: 5.0),
                                ListTile(
                                  leading: Icon(MdiIcons.shield),
                                  title: Text('Defense'),
                                  trailing:
                                      Text(snapshot.data.defense.toString()),
                                ),
                                SizedBox(height: 5.0),
                              ],
                            );
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: StreamBuilder(
                          stream: HeroBlocFactory.heroBloc.heroStream,
                          builder:
                              (context, AsyncSnapshot<Hero.Hero> snapshot) {
                            if (!snapshot.hasData) return Container();
                            return Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'Hero Status',
                                  style: TextStyle(
                                    fontSize: 25.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                SizedBox(height: 15.0),
                                ListTile(
                                  leading: Icon(Icons.account_circle),
                                  title: Text('Type'),
                                  trailing: Text(snapshot.data.type),
                                ),
                                SizedBox(height: 5.0),
                                ListTile(
                                  leading: Icon(MdiIcons.swordCross),
                                  title: Text('Attack'),
                                  trailing:
                                      Text(snapshot.data.damage.toString()),
                                ),
                                SizedBox(height: 5.0),
                                ListTile(
                                  leading: Icon(MdiIcons.shield),
                                  title: Text('Defense'),
                                  trailing:
                                      Text(snapshot.data.defense.toString()),
                                ),
                                SizedBox(height: 5.0),
                              ],
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Column(
                    children: [
                      StreamBuilder(
                        stream: HeroBlocFactory.heroBloc.enemyStream,
                        builder: (context, AsyncSnapshot<Enemy> snapshot) {
                          if (!snapshot.hasData) return Container();
                          final Enemy data = snapshot.data;
                          return Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Stack(
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        AnimatedContainer(
                                          duration: Duration(milliseconds: 250),
                                          width: (data.hp / data.maxHP) *
                                              kHealthBarWidth,
                                          height: 25,
                                          color: Colors.green,
                                        ),
                                        SizedBox(height: 15),
                                        AnimatedContainer(
                                          duration: Duration(milliseconds: 250),
                                          width: ((data.maxHP - data.hp) /
                                                  data.maxHP) *
                                              kHealthBarWidth,
                                          height: 25,
                                          color: Colors.red,
                                        ),
                                      ],
                                    ),
                                    AnimatedContainer(
                                      padding: EdgeInsets.only(top: 4),
                                      duration: Duration(milliseconds: 250),
                                      child: Center(
                                        child:
                                            Text('${data.hp} / ${data.maxHP}'),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 15),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    data.skillCooldown > 0
                                        ? CircularPercentIndicator(
                                            radius: 35.0,
                                            animation: true,
                                            animationDuration: 250,
                                            backgroundColor: Colors.cyan,
                                            lineWidth: 5.0,
                                            percent: data.skillCooldown
                                                    .toDouble() /
                                                kEnemySkillCooldown.toDouble(),
                                            center:
                                                Text('${data.skillCooldown}'),
                                          )
                                        : IconButton(
                                            icon: FaIcon(
                                              FontAwesomeIcons.book,
                                              color: Colors.cyan,
                                              size: 35,
                                            ),
                                            onPressed: null,
                                          ),
                                    SizedBox(width: 100),
                                    IconButton(
                                      icon: Icon(
                                        MdiIcons.swordCross,
                                        size: 35,
                                        color: Colors.cyan,
                                      ),
                                      onPressed: null,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                      StreamBuilder(
                        stream: HeroBlocFactory.heroBloc.battleLogStream,
                        builder: (context, AsyncSnapshot<String> snapshot) {
                          return Expanded(
                            child: Center(
                              child: Text(
                                snapshot.data ?? '',
                                style: TextStyle(
                                  fontSize: 38,
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                      StreamBuilder(
                        stream: HeroBlocFactory.heroBloc.heroStream,
                        builder: (context, AsyncSnapshot<Hero.Hero> snapshot) {
                          if (!snapshot.hasData) return Container();
                          final Hero.Hero data = snapshot.data;
                          return Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    IconButton(
                                      icon: Icon(MdiIcons.sword,
                                          size: 35, color: Colors.cyan),
                                      onPressed: statusData != 0
                                          ? null
                                          : () {
                                              HeroBlocFactory.heroBloc.attack();
                                            },
                                    ),
                                    SizedBox(width: 100),
                                    data.spell.cooldownCounter > 0
                                        ? CircularPercentIndicator(
                                            radius: 35.0,
                                            animation: true,
                                            animationDuration: 250,
                                            lineWidth: 5.0,
                                            backgroundColor: Colors.cyan,
                                            percent: data.spell.cooldownCounter
                                                    .toDouble() /
                                                data.spell.cooldown,
                                            center: Text(
                                                '${data.spell.cooldownCounter}'),
                                          )
                                        : IconButton(
                                            icon: FaIcon(
                                              FontAwesomeIcons.book,
                                              color: Colors.cyan,
                                              size: 35,
                                            ),
                                            onPressed: statusData != 0
                                                ? null
                                                : () {
                                                    HeroBlocFactory.heroBloc
                                                        .castSpell();
                                                  },
                                          ),
                                  ],
                                ),
                                SizedBox(height: 15),
                                Stack(
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        AnimatedContainer(
                                          duration: Duration(milliseconds: 250),
                                          width: (data.hp / data.maxHP) *
                                              kHealthBarWidth,
                                          height: 25,
                                          color: Colors.green,
                                        ),
                                        AnimatedContainer(
                                          duration: Duration(milliseconds: 250),
                                          width: ((data.maxHP - data.hp) /
                                                  data.maxHP) *
                                              kHealthBarWidth,
                                          height: 25,
                                          color: Colors.red,
                                        ),
                                      ],
                                    ),
                                    AnimatedContainer(
                                      padding: EdgeInsets.only(top: 4),
                                      duration: Duration(milliseconds: 250),
                                      child: Center(
                                        child:
                                            Text('${data.hp} / ${data.maxHP}'),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 15),
                                Stack(
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        AnimatedContainer(
                                          duration: Duration(milliseconds: 250),
                                          width: (data.mana / data.maxMana) *
                                              kHealthBarWidth,
                                          height: 25,
                                          color: Colors.blue,
                                        ),
                                        AnimatedContainer(
                                          duration: Duration(milliseconds: 250),
                                          width: ((data.maxMana - data.mana) /
                                                  data.maxMana) *
                                              kHealthBarWidth,
                                          height: 25,
                                          color: Colors.grey,
                                        ),
                                      ],
                                    ),
                                    AnimatedContainer(
                                      padding: EdgeInsets.only(top: 4),
                                      duration: Duration(milliseconds: 250),
                                      child: Center(
                                        child: Text(
                                            '${data.mana} / ${data.maxMana}'),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Use item',
                          style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 15.0),
                        StreamBuilder(
                          stream: HeroBlocFactory.heroBloc.itemsStream,
                          builder:
                              (context, AsyncSnapshot<List<Item>> snapshot) {
                            List<Item> items =
                                snapshot.hasData ? snapshot.data : [null];
                            return DropdownButton<int>(
                              isExpanded: true,
                              value: dropDownValue,
                              items: this._listDropwdownMenu(items),
                              onChanged: (value) {
                                setState(() {
                                  dropDownValue = value;
                                });
                              },
                            );
                          },
                        ),
                        SizedBox(height: 15.0),
                        RaisedButton(
                          child: Text('Use item',
                              style: TextStyle(color: Colors.black)),
                          color: Colors.cyan,
                          disabledColor: Colors.cyan,
                          onPressed: dropDownValue == 0
                              ? null
                              : () {
                                  //TODO use item
                                  HeroBlocFactory.heroBloc
                                      .useItem(dropDownValue);
                                  setState(() {
                                    dropDownValue = 0;
                                  });
                                },
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
