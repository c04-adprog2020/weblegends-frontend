import 'package:flutter/material.dart';
import 'package:weblegends/bloc/account_bloc.dart';
import 'package:weblegends/main.dart';
import 'package:weblegends/models/account.dart';

class MyResultPage extends StatefulWidget {
  MyResultPage({Key key}) : super(key: key);

  @override
  _MyResultPageState createState() => _MyResultPageState();
}

class _MyResultPageState extends State<MyResultPage> {
  bool _isExitLoading = false, _isNextLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.games),
          onPressed: () {
            Navigator.pushReplacementNamed(context, '/');
          },
        ),
        actions: [
          MyNavbarItem(
            text: 'Logout',
            onPressHandler: () {
              AccountBlocFactory.accountBloc.logoutAccount();
              Navigator.pushReplacementNamed(context, '/');
            },
          ),
        ],
      ),
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'You Won!, Your hero has been upgraded',
                style: TextStyle(fontSize: 35, color: Colors.green),
              ),
              SizedBox(height: 30),
              _isExitLoading
                  ? CircularProgressIndicator()
                  : RaisedButton(
                      onPressed: () async {
                        setState(() {
                          _isExitLoading = true;
                        });
                        await AccountFactory.completeLevel();
                        Navigator.pushReplacementNamed(context, '/');
                      },
                      child: Text('Save game & exit'),
                    ),
              _isNextLoading
                  ? CircularProgressIndicator()
                  : RaisedButton(
                      onPressed: () async {
                        setState(() {
                          _isNextLoading = true;
                        });
                        await AccountFactory.nextGame();
                        Navigator.pushReplacementNamed(context, '/battle');
                      },
                      child: Text('Go to the next level'),
                    ),
            ],
          ),
        ),
      ),
    );
  }
}
