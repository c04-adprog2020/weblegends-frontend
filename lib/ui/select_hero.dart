import 'package:flutter/material.dart';
import 'package:weblegends/const.dart';
import 'package:weblegends/models/account.dart';

Future<String> showHeroesSelector(BuildContext context) {
  //TODO add hero description
  return showDialog<String>(
    context: context,
    builder: (context) {
      String dropDownValue = 'Archer';
      bool isLoading = false;
      return StatefulBuilder(
        builder: (context, setState) => AlertDialog(
          title: Text('Select Hero'),
          actions: [
            isLoading
                ? CircularProgressIndicator()
                : FlatButton(
                    onPressed: () async {
                      setState(() {
                        isLoading = true;
                      });
                      final result =
                          await AccountFactory.startNewGame(dropDownValue);
                      Navigator.pop(context, result);
                      Navigator.pushNamed(context, '/battle');
                    },
                    child: Text("Select"),
                  ),
          ],
          actionsPadding: EdgeInsets.all(0),
          content: Container(
            height: 50,
            child: Column(
              children: [
                // Expanded(child: Text("Username")),
                Expanded(
                  child: DropdownButton<String>(
                    value: dropDownValue,
                    items: kHeroList
                        .map<DropdownMenuItem<String>>(
                          (String value) => DropdownMenuItem(
                            value: value,
                            child: Text(value),
                          ),
                        )
                        .toList(),
                    onChanged: (newValue) {
                      setState(() {
                        dropDownValue = newValue;
                      });
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    },
    barrierDismissible: true,
  );
}
